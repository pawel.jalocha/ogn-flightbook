import redis
import sys
import json
from datetime import datetime, timedelta, timezone
import click

import setting


def print_fix(fix, diff):
    tz = timezone(timedelta(hours=1))
    hms = datetime.fromtimestamp(fix['tsp'], tz=tz).strftime('%H:%M:%S')
    line_items = ("{} +({})".format(hms, diff), fix['gsp'], fix['clr'], fix['alt'])
    line = (''.join(['{:15}'.format(item) for item in line_items]))
    print(line)


@click.command()
@click.argument('address')
@click.argument('timestamp')
@click.option('-d', '--delay', type=int)
def debug(address, timestamp, delay):
    pool = redis.ConnectionPool(host=setting.REDIS_HOST, port=setting.REDIS_PORT, db=setting.REDIS_DB,
                                decode_responses=True, encoding="utf-8")
    red = redis.Redis(connection_pool=pool)
    timestamp = int(timestamp)
    if delay is None:
        delay = 60

    key = "BA:{}".format(address)
    if red.exists('QBA') != 0:
        print('address not exists in db')
        sys.exit(1)
    records = red.llen(key)

    old_tsp = 0
    for index in range(records):
        fix_ = json.loads(red.lindex(key, index))
        if fix_['tsp'] > (timestamp - delay):
            if fix_['tsp'] > timestamp + delay:
                break
            tsp_diff = fix_['tsp'] - old_tsp if old_tsp != 0 else 0
            old_tsp = fix_['tsp']
            print_fix(fix_, tsp_diff)


if __name__ == "__main__":
    debug()
